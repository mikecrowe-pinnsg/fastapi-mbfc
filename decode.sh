#!/usr/bin/env bash

# jq '. | [to_entries[] | {"const": .key, "description": .value} ]'  data/$1.json

jq '. | map({"const": .bias, "description": .name})'  data/$1.json

