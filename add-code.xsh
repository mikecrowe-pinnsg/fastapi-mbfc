import os 
import json
from pprint import pprint

def json2yaml(j):
    json.dump(j, open("/tmp/t.json", "w"))
    yml = $(json2yaml -d 16 -i 4 /tmp/t.json)
    return yml

def yaml2json(y):
    open("/tmp/t.yaml", "w").write(y)
    j = json.loads($(yaml2json -p -i 4 /tmp/t.yaml))
    return j

def schema2oas3(fil):
    yml = $(supermodel schema oas3 @(fil)).replace("supermodelIoDrmikecrowe", "")
    open("/tmp/t.yml", "w").write(yml)
    j = json.loads($(yaml2json /tmp/t.yml))
    return j


def ary2obj(ary, title, description, example):
    return {
        "type": "string",
        "title": title,
        "description": description,
        "example": example,
        "enum": ary
    }

def dumpSchema(typ, which, ary):
    base = f"{typ}{which}"
    j = {
        "$id": f"http://supermodel.io/drmikecrowe/{base}",
        "$schema": "http://json-schema.org/draft-07/schema#",
        "title": f"{typ}-{which.lower()}", 
        "description": f"Allowed enums for {typ}",
        "type": "string",
        "enum": ary 
    }
    open(f"supermodel/drmikecrowe/{base}.yaml", "w").write(json2yaml(j))

enums = {}

biases = json.load(open("source/biases.json", "r"))
e_bias_code = []
e_bias_full = []
for obj in biases:
    obj["code"] = "".join(x[0] for x in obj["bias"].upper().split("-"))
    e_bias_code.append(obj["code"])
    e_bias_full.append(obj["bias"])
json.dump(biases, open("data/biases.json", "w"), indent=4)
dumpSchema("bias", "Enums", e_bias_full)
dumpSchema("bias", "Codes", e_bias_code)

schemas = {
    "Error": {
        "type": "object",
        "properties": {
        "code": {
            "type": "string"
        },
        "message": {
            "type": "string"
        }
        },
        "required": [
        "code\n- message"
        ]
    },
}
for f in ["Bias", "Site", "Credibility", "Questionable", "Reporting", "Traffic"]:
    schemas[f"{f}Array"] = {
        "type": "array",
        "items": {
            "$ref": f"#/components/schemas/{f}Model"
        }
    }

schemas["BiasEnums"] = ary2obj(e_bias_full, "BiasEnum", "The bias designator (full width)", "center")
schemas["BiasCodes"] = ary2obj(e_bias_code, "BiasCode", "The shortened bias code (for compact JSON)", "C")

e_code = {}
e_full = {}
for typ in ["credibility", "questionable", "traffic", "reporting"]:
    ucase = typ[0].upper() + typ[1:]
    e_code = []
    e_full = []
    obj = json.load(open(f"source/{typ}.json", "r"))
    ary = []
    for k, v in obj.items():
        full = v.lower().replace(" ", "-")
        if k == "conspiracy":
            k = "conspiracy-pseudoscience"
        e_code.append(k)
        e_full.append(full)
        ary.append({
            typ: full,
            "code": k,
            "name": f"{ucase}: {v}",
            "pretty": typ
        })
    json.dump(ary, open(f"data/{typ}.json", "w"), indent=4)
    schemas[f"{ucase}Enums"] = ary2obj(e_full, f"{ucase}Enums", f"The {ucase} designator (full width)", e_full[0])
    schemas[f"{ucase}Codes"] = ary2obj(e_code, f"{ucase}Codes", f"The shortened {ucase} designator (for compact JSON)", e_code[0])
    dumpSchema(typ, "Enums", e_full)
    dumpSchema(typ, "Codes", e_code)


schemas["BiasModel"] = schema2oas3("supermodel/drmikecrowe/biasModel.yaml")["schemas"]["BiasModel"]
schemas["SiteModel"] = schema2oas3("supermodel/drmikecrowe/siteModel.yaml")["schemas"]["SiteModel"]
for typ in ["credibility", "questionable", "traffic", "reporting"]:
    model = typ[0].upper() + typ[1:] + "Model"
    schemas[model] = schema2oas3(f"supermodel/drmikecrowe/{typ}Model.yaml")["schemas"][model]
    

j = {
    "components": {
       "schemas": schemas
    }
}
yml = json2yaml(j)
open("schema/components.yaml", "w").write(yml)

openapi = open("openapi.yml", "r").read()
j = yaml2json(openapi)
j["components"]["schemas"] = schemas 
for typ in ["site", "bias", "credibility", "questionable", "traffic", "reporting"]:
    utyp = typ[0].upper() + typ[1:]
    j['paths']["/" + typ] = {
        "get": {
            "responses": {
                "200": {
                    "description": "OK",
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": f"#/components/schemas/{utyp}Array"
                            }
                        }
                    }
                },
                "401": {
                    "$ref": "#/components/responses/Unauthorized"
                },
                "404": {
                    "$ref": "#/components/responses/NotFound"
                }
            }
        }
    }
open("openapi.yml", "w").write(json2yaml(j))

def loadCodejs(fil, key):
    j = json.load(open(fil, "r"))
    ret = {}
    for o in j:
        ret[o["code"]] = o[key]
    return ret

biases = loadCodejs("data/biases.json", "bias")
credibility = loadCodejs("data/credibility.json", "credibility")
questionable = loadCodejs("data/questionable.json", "questionable")
traffic = loadCodejs("data/traffic.json", "traffic")
reporting = loadCodejs("data/reporting.json", "reporting")


j = json.load(open("source/combined.json", "r"))
data = []
for k, v in j["sources"].items():
    if "d" not in v:
        continue
    o = {
        "bias": biases[v["b"]],
        "domain": v["d"],
        "name": v["n"],
        "url": v["u"],
        "questionable": [questionable[x] for x in v["q"]]
    }
    if "P" in v:
        o["popularity"] = v["P"]
    if "c" in v:
        o["credibility"] = credibility[v["c"]]
    if "r" in v:
        o["reporting"] = reporting[v["r"]]
    if "a" in v:
        o["traffic"] = traffic[v["a"]]
    data.append(o)
json.dump(data, open("data/sources.json", "w"), indent=4)