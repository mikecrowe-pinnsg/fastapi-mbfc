# generated by fastapi-codegen:
#   filename:  openapi.yml
#   timestamp: 2022-12-05T12:11:58+00:00

from __future__ import annotations

from enum import Enum
from typing import List, Optional

from pydantic import BaseModel, Field


class Error(BaseModel):
    code: Optional[str] = None
    message: Optional[str] = None


class BiasEnums(Enum):
    left = 'left'
    left_center = 'left-center'
    center = 'center'
    right_center = 'right-center'
    right = 'right'
    pro_science = 'pro-science'
    conspiracy_pseudoscience = 'conspiracy-pseudoscience'
    satire = 'satire'
    fake_news = 'fake-news'


class BiasCodes(Enum):
    L = 'L'
    LC = 'LC'
    C = 'C'
    RC = 'RC'
    R = 'R'
    PS = 'PS'
    CP = 'CP'
    S = 'S'
    FN = 'FN'


class CredibilityEnums(Enum):
    high_credibility = 'high-credibility'
    medium_credibility = 'medium-credibility'
    low_credibility = 'low-credibility'
    n_a = 'n/a'


class CredibilityCodes(Enum):
    H = 'H'
    M = 'M'
    L = 'L'
    NA = 'NA'


class QuestionableEnums(Enum):
    n_a = 'n/a'
    conspiracy = 'conspiracy'
    propaganda = 'propaganda'
    poor_sourcing = 'poor-sourcing'
    lack_of_transparency = 'lack-of-transparency'
    failed_fact_checks = 'failed-fact-checks'
    false_claims = 'false-claims'
    misinformation = 'misinformation'
    hate = 'hate'
    plagiarism = 'plagiarism'
    fake_news = 'fake-news'
    pseudoscience = 'pseudoscience'
    imposter = 'imposter'
    censorship = 'censorship'


class QuestionableCodes(Enum):
    NA = 'NA'
    C = 'C'
    P = 'P'
    PS = 'PS'
    LT = 'LT'
    FFC = 'FFC'
    FC = 'FC'
    M = 'M'
    H = 'H'
    PG = 'PG'
    FN = 'FN'
    PSE = 'PSE'
    IM = 'IM'
    CEN = 'CEN'


class TrafficEnums(Enum):
    no_data = 'no-data'
    minimal_traffic = 'minimal-traffic'
    medium_traffic = 'medium-traffic'
    high_traffic = 'high-traffic'


class TrafficCodes(Enum):
    N = 'N'
    L = 'L'
    M = 'M'
    H = 'H'


class ReportingEnums(Enum):
    very_high = 'very-high'
    high = 'high'
    mostly_factual = 'mostly-factual'
    mixed = 'mixed'
    low = 'low'
    very_low = 'very-low'


class ReportingCodes(Enum):
    VH = 'VH'
    H = 'H'
    MF = 'MF'
    M = 'M'
    L = 'L'
    VL = 'VL'


class BiasModel(BaseModel):
    bias: BiasEnums
    name: str = Field(..., example='Center', title='Name')
    description: str = Field(
        ...,
        example='These media sources are moderately to strongly biased toward liberal causes through story selection and/or political affiliation.  They may utilize strong loaded words (wording that attempts to influence an audience by using appeal to emotion or stereotypes), publish misleading reports and omit reporting of information that may damage liberal causes. Some sources in this category may be untrustworthy.',
        title='Description',
    )
    url: str = Field(..., example='https://mediabiasfactcheck.com/center/', title='URL')
    pretty: str = Field(..., example='Center', title='Pretty')
    code: BiasCodes


class SiteModel(BaseModel):
    bias: BiasEnums
    domain: str = Field(..., example='wikipedia.org', title='Domain')
    name: str = Field(..., example='Wikipedia', title='Name')
    reporting: Optional[ReportingEnums] = None
    url: str = Field(
        ..., example='https://mediabiasfactcheck.com/wikipedia/', title='URL'
    )
    popularity: Optional[int] = Field(None, example=97, title='Popularity')
    credibility: Optional[CredibilityEnums] = None
    traffic: Optional[TrafficEnums] = None
    questionable: List[QuestionableEnums] = Field(..., title='Questionable')


class CredibilityModel(BaseModel):
    credibility: CredibilityEnums
    code: CredibilityCodes
    name: str
    pretty: str


class QuestionableModel(BaseModel):
    questionable: QuestionableEnums
    code: QuestionableCodes
    name: str
    pretty: str


class TrafficModel(BaseModel):
    traffic: TrafficEnums
    code: TrafficCodes
    name: str
    pretty: str


class ReportingModel(BaseModel):
    reporting: ReportingEnums
    code: ReportingCodes
    name: str
    pretty: str


class BiasArray(BaseModel):
    __root__: List[BiasModel]


class SiteArray(BaseModel):
    __root__: List[SiteModel]


class CredibilityArray(BaseModel):
    __root__: List[CredibilityModel]


class QuestionableArray(BaseModel):
    __root__: List[QuestionableModel]


class ReportingArray(BaseModel):
    __root__: List[ReportingModel]


class TrafficArray(BaseModel):
    __root__: List[TrafficModel]
